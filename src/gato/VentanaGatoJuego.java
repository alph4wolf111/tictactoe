
package gato;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class VentanaGatoJuego extends JFrame{
    
    private JPanel panelBotones; // Panel para darle orden a matriz
    private JButton posiciones[][]; // Matriz de botones
    private String jugador1 = "Jugador X",jugador2 = "Jugador O"; // Nombre de jugadores
    private static int turno = 0; // Número de turno // Es global
    
    
    
      //Campos para fuente
    private String nombreFuente;
    private int tipoFuente;
    private int tamanioFuente;
    private Font FuentePrimaria;
    private Font FuenteGanador;
    
    // Campos para el menu
    private JMenuBar barraMenus;
    private JMenu menuArchivo;
    private JMenuItem itemAcercaDe;
    private JMenuItem itemSalir;
    private JMenuItem itemSalirGuardar;
    private JMenu menuJuego;
    private JMenuItem itemJuegoNuevo;

    
    public VentanaGatoJuego(String titulo){
        super(titulo);
        
          this.setLayout(new GridLayout()); // Gestor de contenido
          
          
         GestorEventosGato geg = new GestorEventosGato(); // Método escucha
   
         panelBotones = new JPanel (new GridLayout(3,3)); // Orden de texto en panel
         
         
         posiciones = new JButton[3][3]; // Posiciones matriz 3x3
         for (int i = 0; i < 3; i++) { // i Recorre fila // j Recorre columna
             for (int j = 0; j < 3; j++) {
                 posiciones[i][j] = new JButton(); // Se crea boton en matriz
                 posiciones[i][j].addActionListener(geg); // Se añade método escucha
                 panelBotones.add(posiciones[i][j]); // Se añaden al panel
                 
             } // for j
            
        } // for i
         
        
         
         
        this.add(panelBotones); // Añadir panel a ventana
        
         turno = 1;
         
         // Valores de fuente
        nombreFuente = "SansSerif";
        tipoFuente = Font.BOLD;
        tamanioFuente = 32;
        FuenteGanador = new Font(nombreFuente, tipoFuente, tamanioFuente); // Fuente para ganador
        FuentePrimaria = new Font(nombreFuente, Font.PLAIN, 26); // Fuente por defecto  
         

         
         // Barra Menus
        itemAcercaDe = new JMenuItem("Acerca de...");
        itemSalir = new JMenuItem("Salir");
        itemSalirGuardar = new JMenuItem("Salir y Guardar");
        menuArchivo = new JMenu("Archivo");
        menuJuego = new JMenu("Juego");
        itemJuegoNuevo = new JMenuItem("Juego Nuevo");
        
        // Item a menú Archivo
        menuArchivo.add(itemAcercaDe);
        menuArchivo.addSeparator();
        menuArchivo.add(itemSalirGuardar);
        menuArchivo.add(itemSalir);
        
        
        // Item a menú juego
        menuJuego.add(itemJuegoNuevo);
        
        
        // Menús a barra de menús
        barraMenus = new JMenuBar();
        barraMenus.add(menuArchivo);
        barraMenus.add(menuJuego);
        
        // Agregar barraMenus
        this.setJMenuBar(barraMenus);
        
        
        // Se carga partida
        
        cargarConfiguracion();
        
         // Gestión de eventos de elementos del menú
        itemAcercaDe.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                // Mostrar ventana emergente con información acerca de la aplicación
                JOptionPane.showMessageDialog(
                                VentanaGatoJuego.this, //VentanaPadre
                                "Juego del Gato\nAntonio de Jesús García López\nCUCEA\n2018A", // Mensaje
                                "Acerca de Juego del Gato", // Título de la ventana
                                JOptionPane.PLAIN_MESSAGE // Tipo de mensaje
                );
            } // actionPerformed
        });
        itemSalir.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                int resp = JOptionPane.showConfirmDialog(
                        VentanaGatoJuego.this, //VentanaPadre
                        "¿Estás seguro que deseas salir de la aplicación? \n (Los cambios no se guardarán ** se reiniciará la partida **)", // Mensaje
                        "Confirmación",// Título de la ventana
                        JOptionPane.YES_NO_OPTION, // Tipo de mensaje
                        JOptionPane.QUESTION_MESSAGE
                );
                if(resp == JOptionPane.YES_OPTION){
                    for (int i = 0; i < 3; i++) {  // i Recorre fila // j Recorre columna
                        for (int j = 0; j < 3; j++) {
                        posiciones[i][j].setText(""); // Rellena botones a texto vacío
                        turno = 1; // Turno de jugador 1
                        posiciones[i][j].setEnabled(true); // Activa los botones
                        } // for j
                    } //for i
                    guardarConfiguracion();
                    System.exit(0);
                } // if
            } // actionPerformed
        });
        
        itemSalirGuardar.addActionListener(new ActionListener(){  // Salir y guardar
            @Override
            public void actionPerformed(ActionEvent ae) {
                int resp = JOptionPane.showConfirmDialog(
                        VentanaGatoJuego.this, //VentanaPadre
                        "¿Estás seguro que deseas salir de la aplicación?", // Mensaje
                        "Confirmación",// Título de la ventana
                        JOptionPane.YES_NO_OPTION, // Tipo de mensaje
                        JOptionPane.QUESTION_MESSAGE
                );
                if(resp == JOptionPane.YES_OPTION){
                    guardarConfiguracion();
                    System.exit(0);
                } // if
            } // actionPerformed
        });
        
        
        
        itemJuegoNuevo.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                 for (int i = 0; i < 3; i++) {  // i Recorre fila // j Recorre columna
                    for (int j = 0; j < 3; j++) {
                    posiciones[i][j].setText(""); // Rellena botones a texto vacío
                    turno = 1; // Turno de jugador 1
                    posiciones[i][j].setEnabled(true); // Activa los botones
             } // for j
            
        } //for i
         
            } // actionPerformed
        });
        
          
        
          
    } // Fin constructor

    private class GestorEventosGato implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent ae) {
            
            
            JButton botonSelec = (JButton) ae.getSource(); // Detecta y guarda el botón que origino la acción
            
            
            
            
                if(turno == 2){
                VentanaGatoJuego.this.setTitle(jugador1); // Cambia nombre de ventana por turno de jugador
            }
            else{
                VentanaGatoJuego.this.setTitle(jugador2); // Cambia nombre de ventana por turno de jugador
            }
            
            if(turno == 1){ 
                if (botonSelec.getText().equals("")) { // Verifica que este vacía
                    turno = 2;
                    botonSelec.setText("X"); // Pone como texto  X
                    botonSelec.setFont(FuentePrimaria); // Texto con fuente primaria
                    botonSelec.setEnabled(false); // Desactiva botón
                    
                    
                    
                    
                }
                
            } // if 
            if(turno == 2){ 
                if (botonSelec.getText().equals("")) { // Verifica que este vacía
                    turno = 1;
                    botonSelec.setText("O"); // Pone como texto  O
                    botonSelec.setFont(FuentePrimaria); // Texto con fuente primaria
                    botonSelec.setEnabled(false); // Desactiva botón
                    
                    
                    
                }
            } // if 
            
            
            
            
            
            
            
            
        verificar();    
        } // Action performed
        
        
        

        

        
    }// GestorEventosGato
    
    public  void verificar() {
        int ganador = 0;
        
        
        for (int i = 0; i < 3; i++) {  // Recorre posiciones a verificar  
            
            // Verifica jugador X
            if(posiciones[0][i].getText().equals("X") && posiciones[1][i].getText().equals("X")&& // Verifica Eje Vertical
                    posiciones[2][i].getText().equals("X")){
                ganador = 1;
                posiciones[0][i].setFont(FuenteGanador);
                posiciones[1][i].setFont(FuenteGanador);
                posiciones[2][i].setFont(FuenteGanador);
            }
            if(posiciones[i][0].getText().equals("X") && posiciones[i][1].getText().equals("X")&& // Verifica Eje Horizontal
                    posiciones[i][2].getText().equals("X")){
                ganador = 1;
                posiciones[i][0].setFont(FuenteGanador);
                posiciones[i][1].setFont(FuenteGanador);
                posiciones[i][2].setFont(FuenteGanador);
            }
        } // for
            
            if (posiciones[0][0].getText().equals("X") && posiciones[1][1].getText().equals("X")&& // Verifica Eje Diagonal
                    posiciones[2][2].getText().equals("X"))
            {
                ganador = 1;
                posiciones[0][0].setFont(FuenteGanador);
                posiciones[1][1].setFont(FuenteGanador);
                posiciones[2][2].setFont(FuenteGanador);
            }
            if (posiciones[0][2].getText().equals("X") && posiciones[1][1].getText().equals("X")&& // Verifica Eje Diagonal
                    posiciones[2][0].getText().equals("X"))
            {
                ganador = 1;
                posiciones[0][2].setFont(FuenteGanador);
                posiciones[1][1].setFont(FuenteGanador);
                posiciones[2][0].setFont(FuenteGanador);
            }
            
            // Verifica jugador O
            for (int i = 0; i < 3; i++) {  // Recorre posiciones a verificar 
            if(posiciones[0][i].getText().equals("O") && posiciones[1][i].getText().equals("O")&& // Verifica Eje Vertical
                    posiciones[2][i].getText().equals("O")){
                ganador = 2;
                posiciones[0][i].setFont(FuenteGanador);
                posiciones[1][i].setFont(FuenteGanador);
                posiciones[2][i].setFont(FuenteGanador);
            } // if
            if(posiciones[i][0].getText().equals("O") && posiciones[i][1].getText().equals("O")&& // Verifica Eje Horizontal
                    posiciones[i][2].getText().equals("O")){
                ganador = 2;
                posiciones[i][0].setFont(FuenteGanador);
                posiciones[i][1].setFont(FuenteGanador);
                posiciones[i][2].setFont(FuenteGanador);
            }// if
            } //  for
            
            if (posiciones[0][0].getText().equals("O") && posiciones[1][1].getText().equals("O")&& // Verifica Eje Diagonal
                    posiciones[2][2].getText().equals("O"))
            {
                ganador = 2;
                posiciones[0][0].setFont(FuenteGanador);
                posiciones[1][1].setFont(FuenteGanador);
                posiciones[2][2].setFont(FuenteGanador);
            } // if
            if (posiciones[0][2].getText().equals("O") && posiciones[1][1].getText().equals("O")&& // Verifica Eje Diagonal
                    posiciones[2][0].getText().equals("O"))
            { 
                ganador = 2;
                posiciones[0][2].setFont(FuenteGanador);
                posiciones[1][1].setFont(FuenteGanador);
                posiciones[2][0].setFont(FuenteGanador);
            } // if 
            
            if(ganador == 1){
                JOptionPane.showMessageDialog(
                        VentanaGatoJuego.this, //VentanaPadre
                        "¡Felicidades jugador X has ganado!", // Mensaje
                        "Ganador",// Título de la ventana
                        JOptionPane.INFORMATION_MESSAGE
                        
                );
            } // if 
            if(ganador == 2){
                JOptionPane.showMessageDialog(
                        VentanaGatoJuego.this, //VentanaPadre
                        "¡Felicidades jugador O has ganado!", // Mensaje
                        "Ganador",// Título de la ventana
                        JOptionPane.INFORMATION_MESSAGE
                        
                );
            } // if 
            
            if(ganador !=0){ // Desactiva botones
                for (int i = 0; i < 3; i++) { // i Recorre fila // j Recorre columna
                    for (int j = 0; j < 3; j++) {
                    posiciones[i][j].setEnabled(false);
                    } // for j
                } // for i
                
            } // if 

            

        
    } // Verificar
    
    
    public void guardarConfiguracion(){ // Método para guardar movimientos del juego
        Formatter salidaArch;
        try{
            salidaArch = new Formatter("partida.txt"); // Archivo donde estarán los movimientos y turno de la partida 
        
        }catch(FileNotFoundException e){
            System.out.println("Error al abrir archivo de configuracion");
            return;     //Salir del metodo
        }
        //Guardar la configuracion
        try {
            
            for (int i = 0; i < 3; i++) {  // i Recorre fila // j Recorre columna
                    for (int j = 0; j < 3; j++) {
                    
                    if(posiciones[i][j].getText().equalsIgnoreCase("X") || posiciones[i][j].getText().equalsIgnoreCase("O"))
                    {
                        salidaArch.format("%s%n",posiciones[i][j].getText());// Obtener movimientos
                        
                    }
                    else {
                        salidaArch.format("%s%n","N"); // Movimientos en blanco se le asigna "N"
                    }
             } // for j
        } //for i
            salidaArch.format("%d%n",turno); // Turno se agrega al final, línea 10
            
        }catch(Exception e){
            System.err.println("Error al escribir en archivo de configuracion");
        }
        
        // Cerrar el archivo
        if(salidaArch != null){
            salidaArch.close();  
        }
    }//Fin de guardarConfiguracion
    
    public void cargarConfiguracion(){ // Método para cargar movimientos del juego
        Scanner entradaArch;
        try{
            Path ruta = Paths.get("partida.txt"); // Archivo donde están movimientos y turno de la partida 
            entradaArch = new Scanner(ruta);
            if(Files.exists(ruta)){
                entradaArch = new Scanner(ruta);
            }else{
                return;
            }
        }catch(IOException e){
        System.err.println("Error al abrir archivo de configuracion.");
        return;
        }
        //Carga (leer del archivo) la configuracion
        try{
            
            
            for (int i = 0; i < 3; i++) {  // i Recorre fila // j Recorre columna
                    for (int j = 0; j < 3; j++) {
                        
                    
                        posiciones[i][j].setText(entradaArch.next()); // Rellena botones a texto vacío 
                        if(posiciones[i][j].getText().equalsIgnoreCase("N")){ // compara si es igual a N (espacios vacíos)
                            posiciones[i][j].setText(""); // Lo rellena con espacios vacíos
                        }
                    
                    
                    
                    if(posiciones[i][j].getText().equalsIgnoreCase("X") || posiciones[i][j].getText().equalsIgnoreCase("O")){// Verifica si están ocupados
                        posiciones[i][j].setFont(FuentePrimaria); // Se le aplica estilo
                        posiciones[i][j].setEnabled(false); // Desactiva los botones
                    }
                    
             } // for j
            
        } //for i
            
            turno = entradaArch.nextInt(); 
            
            verificar(); // Verificar si no ha ganado alguien
            
            
        }catch(Exception e){
            System.out.println("Error al leer archivo de configuracion.");
            return;
        }
        
        //Cerrar el archivo
        if (entradaArch != null){
            entradaArch.close(); 
        }
        
    }   //Fin carga configuracion
    
   
    
    
} // Fin de la clase
