
package gato;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VentanaGato extends JFrame{
    private JPanel panelBotones;
    private JButton btIniciarJuego;
    private JLabel etiquetaBienvenido;
    public VentanaGato(String titulo){
        super(titulo);
        
          this.setLayout(new BorderLayout()); // Gestor de contenido
          panelBotones = new JPanel (new GridLayout(1,1)); // Orden de botones en panel
          btIniciarJuego = new JButton ("Iniciar Juego");  // Botones de menu principal
          etiquetaBienvenido = new JLabel("BIENVENIDO AL  JUEGO DEL GATO");
          etiquetaBienvenido.setFont(new Font("SansSerif", Font.PLAIN, 18 ));
          
          etiquetaBienvenido.setHorizontalAlignment(JLabel.CENTER);
          this.add(etiquetaBienvenido,BorderLayout.CENTER);
          panelBotones.add(btIniciarJuego);  // Agregar botones al panel
          
          this.add(panelBotones, BorderLayout.SOUTH); // Agregar panel a ventana
          
          
          btIniciarJuego.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) { // Partida nueva se crea ventana nueva VentanaJuego Gato
                VentanaGatoJuego vg = new VentanaGatoJuego("Partida");
        
                 vg.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Habilitar cerrar ventana
                 vg.setBounds(50, 50, 350, 350);
                 vg.setVisible(true);   
                 
            } // Fin implementación
        }); // Evento escucha
    }// Fin del constructor
    
    
} // Fin de la clase
